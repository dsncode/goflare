package goflare

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestUpdateRecord(t *testing.T) {

	//Mock server
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		// Send response to be tested
		rw.Write([]byte(`{"success":true}`))
	}))
	// Close the server when test finishes
	defer server.Close()

	proxy := NewProxyClient("admin@dsncode.com", "YOUR_KEY", server.Client())
	proxy.SetAPIURL(server.URL)

	request := UpdateZoneRequest{
		IP:       "1.1.1.1",
		Name:     "prefix",
		ZoneID:   "ZONE_ID",
		RecordID: "RECORD_ID",
		Type:     "A",
	}

	response, err := proxy.UpdateRecord(request)
	if err != nil {
		t.Fatal(err)
	}
	if !response.Success {
		t.Errorf("record could not be updated: %v", response.Errors)
	}
}
