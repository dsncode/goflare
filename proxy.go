package goflare

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

// RecordUpdateURLFormat represent the url where dns records PUT requests will be sent to
const RecordUpdateURLFormat = "%s/client/v4/zones/%s/dns_records/%s"

// APIURL pointing to cloudflare server
const APIURL = "https://api.cloudflare.com"

// ProxyClient helps to update cloudflare service via API
type ProxyClient interface {
	UpdateRecord(updateZoneRequest UpdateZoneRequest) (UpdateZoneResponse, error)
	SetAPIURL(apiurl string)
}

type proxyClient struct {
	apiurl string
	email  string
	key    string
	client *http.Client
}

// UpdateZoneRequest cloudflare payload
type UpdateZoneRequest struct {
	Name     string
	Type     string
	ZoneID   string
	RecordID string
	IP       string
}

type updateZonePayload struct {
	Name    string `json:"name"`
	Content string `json:"content"`
	Type    string `json:"type"`
}

// UpdateZoneResponse cloudflare response
type UpdateZoneResponse struct {
	Success bool `json:"success"`
	Errors  interface{}
}

// NewProxyClient creates a Cloudflare proxy
func NewProxyClient(email string, key string, client *http.Client) ProxyClient {

	return &proxyClient{
		apiurl: APIURL,
		email:  email,
		key:    key,
		client: client,
	}
}

// SetAPIURL override api url
func (proxy *proxyClient) SetAPIURL(apiurl string) {
	proxy.apiurl = apiurl
}

// UpdateRecord updates a cloudflare zone/record
func (proxy proxyClient) UpdateRecord(updateZoneRequest UpdateZoneRequest) (UpdateZoneResponse, error) {

	payload := updateZonePayload{
		Name:    updateZoneRequest.Name,
		Content: updateZoneRequest.IP,
		Type:    updateZoneRequest.Type,
	}

	jsonPayload, _ := json.Marshal(payload)
	bytePayload := bytes.NewBuffer(jsonPayload)

	toPUTURL := fmt.Sprintf(RecordUpdateURLFormat, proxy.apiurl, updateZoneRequest.ZoneID, updateZoneRequest.RecordID)

	req, _ := http.NewRequest(http.MethodPut, toPUTURL, bytePayload)

	req.Header.Set("X-Auth-Email", proxy.email)
	req.Header.Set("X-Auth-Key", proxy.key)
	req.Header.Set("Content-Type", "application/json")

	res, err := proxy.client.Do(req)

	if err != nil {
		return UpdateZoneResponse{}, err
	}

	var data UpdateZoneResponse
	resByte, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return UpdateZoneResponse{}, err
	}

	err = json.Unmarshal(resByte, &data)

	if err != nil {
		return UpdateZoneResponse{}, err
	}

	return data, nil
}
